import {shuffleArray} from './utils/utils';

export type Question = {
    category:string;
    correct_answer:string;
    incorrect_answers:string[];
    difficulty:string;
    question:string;
    type:string;
}

export type QuestionsState = Question & { anwsers: string[] };

export enum Difficulty {
    E="easy",
    M="medium",
    H="hard"
}


export const FetchData = async (amount:number , difficulty:Difficulty) => {
    const endpiont= `https://opentdb.com/api.php?amount=${amount}&difficulty=${difficulty}&type=multiple`;
    const data = await (await fetch(endpiont)).json();
    console.log(data);
    return data.results.map((question:Question) => (
        {
            ...question,
            anwsers: shuffleArray([...question.incorrect_answers , question.correct_answer])
        }
    ))
}