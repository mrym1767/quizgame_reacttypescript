import React, { FunctionComponent, FC } from 'react';
import {AnswerObject} from '../App';
import { Wrapper, ButtonWrapper } from './QuestionCard.Style';


type Props = {
    question : string;
    anwsers : string[];
    userAnwser : AnswerObject | null;
    handleUserAnwser:(e:React.MouseEvent<HTMLButtonElement>)=>void;
    questionNum : number;
    totalQuestion : number;

}

const QuestionCard : FC <Props> = ({question,anwsers,userAnwser,totalQuestion,questionNum,handleUserAnwser}) => {
    return (
        <>
        <Wrapper>
        <div>
            <p className='number'>
                {`${questionNum} / ${totalQuestion}`}
            </p>
        </div>

        <p dangerouslySetInnerHTML={{ __html: question }} ></p>

        {anwsers.map(anwser => (
            <ButtonWrapper
            key={anwser}
            correct={userAnwser?.correctAnswer === anwser}
            userClicked={userAnwser?.answer === anwser}
            >

            <div key={anwser}>
                <button disabled={!!userAnwser} value={anwser} onClick={handleUserAnwser}>
                    <span dangerouslySetInnerHTML={{ __html: anwser }} />
                </button>
            </div>

            </ButtonWrapper>
        ))}
        </Wrapper>
        </>        
    )
}

export default QuestionCard;