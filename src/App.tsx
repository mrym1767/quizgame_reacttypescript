import React, { FunctionComponent, useState } from 'react';
import QuestionCard from './components/QuestionCard';
import {FetchData , Difficulty} from './API';
import {QuestionsState} from './API';
import {GlobalStyle , Wrapper} from './App.style';


export type AnswerObject = {
  question: string;
  answer: string;
  correct: boolean;
  correctAnswer: string;
};


const App  = () => {
  const [loading , setLoading] = useState(false);
  const [questions , setQuestion]  = useState<QuestionsState[]>([]);
  const [number , setNumber] = useState(0);
  const [userAnwser , setUserAnwser] = useState<AnswerObject[]>([]);
  const [score , setScore] = useState(0);
  const [gameover,setGameover] =useState(true);
  const totalQuestions = 10;


  const handleNext = () => {
    if (number+1 !== totalQuestions){
      setNumber(number+1);
    } else setGameover(true);
    
  }
 

  const checkAnwser = (e: React.MouseEvent<HTMLButtonElement>) => {
    if(!gameover) {
      const copyAnswer = [...userAnwser];
      const correct = (e.currentTarget.value===questions[number].correct_answer? true : false)
      copyAnswer.push({
      question:questions[number].question,
      answer:e.currentTarget.value,
      correct,
      correctAnswer:questions[number].correct_answer
    })
    setUserAnwser(copyAnswer);
    if (correct) setScore(score+10);
    }
  }


  const handleStart = async() => {
    setLoading(true);
    const newQuestion = await FetchData(totalQuestions,Difficulty.E);
    setQuestion(newQuestion);
    setScore(0)
    setUserAnwser([]);
    setNumber(0);
    setGameover(false);
    setLoading(false);
  }


  return (
    <>
    <GlobalStyle/>
    <Wrapper>
      <h1>Quiz App</h1>

      {gameover || userAnwser.length===totalQuestions? (
        <button className='start' onClick={handleStart}>start</button>
      ): null}

      {!gameover? (<p className='score'>score:{score}</p>) : null}

      {loading? (<p>loading...</p>) : null}

      {!loading && !gameover? (
        <QuestionCard
        questionNum={number+1}
        totalQuestion={10}
        question={questions[number].question}
        anwsers={questions[number].anwsers}
        userAnwser={userAnwser? userAnwser[number] : null}
        handleUserAnwser={(e:React.MouseEvent<HTMLButtonElement>)=>checkAnwser(e)}
      />
      ): null}

      {!loading && !gameover && userAnwser.length===number+1 && number!==totalQuestions-1 ? 
      (
        <button className='next' onClick={handleNext}>
          Next Question
        </button>
      ) : null}
      </Wrapper>
    </>
  )
}

export default App;
